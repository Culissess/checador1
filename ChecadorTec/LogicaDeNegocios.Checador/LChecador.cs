﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.Checador;
using Entidades.Checador;

namespace LogicaDeNegocios.Checador
{
    public class LChecador
    {
        private AChecada _aChecador;
        public LChecador()
        {
            _aChecador = new AChecada();
        }
        public void GuardarChecada(EChecador checador)
        {
            _aChecador.GuardarChecada(checador);
        }
        public List<EChecador> ObtenerChecada()
        {
            var list = new List<EChecador>();
            list = _aChecador.ObtenerChecada();
            return list;
        }
    }
}
