﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.Checador;
using Entidades.Checador;


namespace LogicaDeNegocios.Checador
{
    public class LTipoPersona
    {
        private ATipopersona _aTipopersona;
        public LTipoPersona()
        {
            _aTipopersona = new ATipopersona();
        }
        public void EliminarTipopersona(int idtipopersona)
        {
            _aTipopersona.EliminarPersona(idtipopersona);
        }
        public void GuardarTipopersona(ETipoPersonal persona)
        {
            _aTipopersona.GuardarPersona(persona);
        }
        public List<ETipoPersonal> ObtenerListaTipopersona(string filtro)
        {
            var list = new List<ETipoPersonal>();
            list = _aTipopersona.ObtenerListaPersona(filtro);
            return list;
        }
    }
}
