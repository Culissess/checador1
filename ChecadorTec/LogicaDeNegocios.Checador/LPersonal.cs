﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.Checador;
using Entidades.Checador;

namespace LogicaDeNegocios.Checador
{
    public class LPersonal
    {
        private APersonal _aPersonal;
        public LPersonal()
        {
            _aPersonal = new APersonal();
        }
        public void EliminarPersonal(string rfc)
        {
            _aPersonal.EliminarPersonal(rfc);
        }
        public void GuardarPersonal(EPersonal personal)
        {
            _aPersonal.GuardarPersonal(personal);
        }
        public List<EPersonal> ObtenerListaPersonal(string filtro)
        {
            var list = new List<EPersonal>();
            list = _aPersonal.ObtenerListaPersonal(filtro);
            return list;
        }
    }
}
