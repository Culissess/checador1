
create table tipopersonal(
idpersonal int auto_increment primary key,
nombre varchar(50));

create table personal(
Rfc varchar(50) primary key,
nombre varchar(25),
apellidoP varchar(25),
apellidoM varchar(25),
fechadenacimiento date,
fktipopersonal int,
foreign key(fktipopersonal) references tipopersonal(idpersonal));

create table checadas(
idChe int auto_increment primary key,
fkrfc varchar(50),
fecha varchar(50),
hora varchar(15),
foreign key (fkrfc)references personal(rfc));

select * from checadas;
Select idche as 'Num Checadas', rfc as RFC, Fecha, Hora from checadas;
insert into checadas values(null, 'asdjn124', 'martes, 27 de julio del 2020', '16:54:44');
drop table checadas;
