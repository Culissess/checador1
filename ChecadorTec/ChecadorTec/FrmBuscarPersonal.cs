﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.Checador;
using LogicaDeNegocios.Checador;

namespace ChecadorTec
{
    public partial class FrmBuscarPersonal : Form
    {
        private LPersonal _lPersonal;
        private EPersonal _ePersonal;
        public FrmBuscarPersonal()
        {
            InitializeComponent();
            _lPersonal = new LPersonal();
            _ePersonal = new EPersonal();
        }
        private void Eliminar()
        {
            string numero = dtgPersonal.CurrentRow.Cells["rfc"].Value.ToString();
            _lPersonal.EliminarPersonal(numero);
        }
        private void BindingPersonal()
        {
            _ePersonal.Rfc = dtgPersonal.CurrentRow.Cells["rfc"].Value.ToString();
            _ePersonal.Nombre = dtgPersonal.CurrentRow.Cells["nombre"].Value.ToString();
            _ePersonal.ApellidoP = dtgPersonal.CurrentRow.Cells["apellidoP"].Value.ToString();
            _ePersonal.ApellidoM = dtgPersonal.CurrentRow.Cells["apellidoM"].Value.ToString();
            _ePersonal.Fechadenacimiento = Convert.ToDateTime(dtgPersonal.CurrentRow.Cells["fechadenacimiento"].Value.ToString());
            _ePersonal.Fktipopersonal = Convert.ToInt32(dtgPersonal.CurrentRow.Cells["fktipopersonal"].Value.ToString());
        }
        private void BuscarPersonal(string filtro)
        {
            dtgPersonal.DataSource = _lPersonal.ObtenerListaPersonal(filtro);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmPersonal personal = new FrmPersonal();
            personal.ShowDialog();
            BuscarPersonal("");
        }

        private void FrmBuscarPersonal_Load(object sender, EventArgs e)
        {
            BuscarPersonal("");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar ese registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarPersonal("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void dtgPersonal_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            BindingPersonal();
            FrmPersonal frmPersona = new FrmPersonal(_ePersonal);
            frmPersona.ShowDialog();
            BuscarPersonal("");
        }
    }
}
