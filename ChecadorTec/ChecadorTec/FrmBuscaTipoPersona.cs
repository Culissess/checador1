﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.Checador;
using LogicaDeNegocios.Checador;

namespace ChecadorTec
{
    public partial class FrmBuscaTipoPersona : Form
    {
        private LTipoPersona _lTipopersona;
        private ETipoPersonal _eTipopersona;
        public FrmBuscaTipoPersona()
        {
            InitializeComponent();
            _lTipopersona = new LTipoPersona();
            _eTipopersona = new ETipoPersonal();
        }

        private void Eliminar()
        {
            int numero = Convert.ToInt32(dtgPersonal.CurrentRow.Cells["idpersonal"].Value.ToString());
            _lTipopersona.EliminarTipopersona(numero);
        }
        private void Bindingpersona()
        {
            _eTipopersona.Idpersonal = Convert.ToInt32(dtgPersonal.CurrentRow.Cells["idpersonal"].Value.ToString());
            _eTipopersona.Nombre = dtgPersonal.CurrentRow.Cells["nombre"].Value.ToString();
        }
        private void BuscarPersona(string filtro)
        {
            dtgPersonal.DataSource = _lTipopersona.ObtenerListaTipopersona(filtro);
        }
        private void FrmPersona_Load(object sender, EventArgs e)
        {
            BuscarPersona("");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar ese registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarPersona("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void dtgPersonal_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Bindingpersona();
            FrmTipoPersonal frmTipoPersona = new FrmTipoPersonal(_eTipopersona);
            frmTipoPersona.ShowDialog();
            BuscarPersona("");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmTipoPersonal persona = new FrmTipoPersonal();
            persona.ShowDialog();
            BuscarPersona("");
        }

        private void dtgPersonal_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
