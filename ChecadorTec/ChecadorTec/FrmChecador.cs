﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.Checador;
using LogicaDeNegocios.Checador;

namespace ChecadorTec
{
    public partial class FrmChecador : Form
    {
        private LChecador _lChecador;
        private EChecador _eChecador;
        private bool _isEnablebinding = false;

        public FrmChecador()
        {
            InitializeComponent();
            _lChecador = new LChecador();
            _eChecador = new EChecador();
            _isEnablebinding = true;
            BindingChecador();
        }
        public FrmChecador(EChecador checador)
        {
            InitializeComponent();
            _lChecador = new LChecador();
            _eChecador = new EChecador();
            _isEnablebinding = true;
            _eChecador = checador;
            BindingChecador();
        }

        private void BindingChecador()
        {
            if (_isEnablebinding)
            {
                if (_eChecador.IdChe == _eChecador.IdChe)
                {
                    _eChecador.IdChe = _eChecador.IdChe;
                }
                _eChecador.Fkrfc = txtControl.Text;
                _eChecador.Fecha = DateTime.Now.ToLongDateString();
                _eChecador.Hora = DateTime.Now.ToLongTimeString();


            }
        }
        private void Guardar()
        {
            _lChecador.GuardarChecada(_eChecador);
        }
        private void BuscarPersona()
        {
            dtgCheck.DataSource = _lChecador.ObtenerChecada();
        }
        private void FrmChecador_Load(object sender, EventArgs e)
        {
            BuscarPersona();
        }

        private void lblFecha_Click(object sender, EventArgs e)
        {

        }

        private void lblHora_Click(object sender, EventArgs e)
        {

        }

        private void tmrHoraF_Tick(object sender, EventArgs e)
        { 
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BindingChecador();
           
            Guardar();
            BuscarPersona();
        }
       private void txtControl_TextChanged(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblHora.Text = DateTime.Now.ToLongTimeString();
            lblFecha.Text = DateTime.Now.ToLongDateString();
        }
    }
}
