﻿namespace ChecadorTec
{
    partial class FrmChecador
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblHora = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.txtControl = new System.Windows.Forms.TextBox();
            this.lblControl = new System.Windows.Forms.Label();
            this.dtgCheck = new System.Windows.Forms.DataGridView();
            this.PtbArriba = new System.Windows.Forms.PictureBox();
            this.PtbIzquierda = new System.Windows.Forms.PictureBox();
            this.ptbLogo = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dtgCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PtbArriba)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PtbIzquierda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(184)))), ((int)(((byte)(184)))));
            this.lblHora.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(57)))), ((int)(((byte)(84)))));
            this.lblHora.Location = new System.Drawing.Point(245, 173);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(89, 37);
            this.lblHora.TabIndex = 20;
            this.lblHora.Text = "Hora";
            this.lblHora.Click += new System.EventHandler(this.lblHora_Click);
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Font = new System.Drawing.Font("Arial Narrow", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(57)))), ((int)(((byte)(84)))));
            this.lblFecha.Location = new System.Drawing.Point(177, 69);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(102, 42);
            this.lblFecha.TabIndex = 19;
            this.lblFecha.Text = "Fecha";
            this.lblFecha.Click += new System.EventHandler(this.lblFecha_Click);
            // 
            // txtControl
            // 
            this.txtControl.Location = new System.Drawing.Point(171, 266);
            this.txtControl.Name = "txtControl";
            this.txtControl.Size = new System.Drawing.Size(694, 20);
            this.txtControl.TabIndex = 16;
            this.txtControl.TextChanged += new System.EventHandler(this.txtControl_TextChanged);
            // 
            // lblControl
            // 
            this.lblControl.AutoSize = true;
            this.lblControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(184)))), ((int)(((byte)(184)))));
            this.lblControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblControl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(57)))), ((int)(((byte)(84)))));
            this.lblControl.Location = new System.Drawing.Point(114, 269);
            this.lblControl.Name = "lblControl";
            this.lblControl.Size = new System.Drawing.Size(42, 16);
            this.lblControl.TabIndex = 15;
            this.lblControl.Text = "RFC:";
            // 
            // dtgCheck
            // 
            this.dtgCheck.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(184)))), ((int)(((byte)(184)))));
            this.dtgCheck.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCheck.Location = new System.Drawing.Point(91, 339);
            this.dtgCheck.Name = "dtgCheck";
            this.dtgCheck.Size = new System.Drawing.Size(1232, 361);
            this.dtgCheck.TabIndex = 18;
            // 
            // PtbArriba
            // 
            this.PtbArriba.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(57)))), ((int)(((byte)(84)))));
            this.PtbArriba.Location = new System.Drawing.Point(665, 0);
            this.PtbArriba.Name = "PtbArriba";
            this.PtbArriba.Size = new System.Drawing.Size(706, 33);
            this.PtbArriba.TabIndex = 2;
            this.PtbArriba.TabStop = false;
            // 
            // PtbIzquierda
            // 
            this.PtbIzquierda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(57)))), ((int)(((byte)(84)))));
            this.PtbIzquierda.Location = new System.Drawing.Point(0, 0);
            this.PtbIzquierda.Name = "PtbIzquierda";
            this.PtbIzquierda.Size = new System.Drawing.Size(38, 750);
            this.PtbIzquierda.TabIndex = 3;
            this.PtbIzquierda.TabStop = false;
            // 
            // ptbLogo
            // 
            this.ptbLogo.Image = global::ChecadorTec.Properties.Resources._102407024_265037261388171_6599045758973736229_n;
            this.ptbLogo.Location = new System.Drawing.Point(890, 36);
            this.ptbLogo.Name = "ptbLogo";
            this.ptbLogo.Size = new System.Drawing.Size(433, 249);
            this.ptbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbLogo.TabIndex = 17;
            this.ptbLogo.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(57)))), ((int)(((byte)(84)))));
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(184)))), ((int)(((byte)(184)))));
            this.button1.Location = new System.Drawing.Point(385, 292);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(198, 41);
            this.button1.TabIndex = 21;
            this.button1.Text = "Checar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FrmChecador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(184)))), ((int)(((byte)(184)))));
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblHora);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.dtgCheck);
            this.Controls.Add(this.ptbLogo);
            this.Controls.Add(this.txtControl);
            this.Controls.Add(this.lblControl);
            this.Controls.Add(this.PtbIzquierda);
            this.Controls.Add(this.PtbArriba);
            this.Name = "FrmChecador";
            this.Text = "Check Tec";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmChecador_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PtbArriba)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PtbIzquierda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.TextBox txtControl;
        private System.Windows.Forms.Label lblControl;
        private System.Windows.Forms.DataGridView dtgCheck;
        private System.Windows.Forms.PictureBox PtbArriba;
        private System.Windows.Forms.PictureBox PtbIzquierda;
        private System.Windows.Forms.PictureBox ptbLogo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer1;
    }
}

