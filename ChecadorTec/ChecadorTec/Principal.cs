﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChecadorTec
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void agregarTipoPersonalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmBuscaTipoPersona personal = new FrmBuscaTipoPersona();
            personal.ShowDialog();

        }

        private void agregarPersonalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmBuscarPersonal personal = new FrmBuscarPersonal();
            personal.ShowDialog();
        }
    }
}
