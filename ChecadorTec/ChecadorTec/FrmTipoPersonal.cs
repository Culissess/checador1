﻿using Entidades.Checador;
using LogicaDeNegocios.Checador;
using System;
using System.Windows.Forms;

namespace ChecadorTec
{
    public partial class FrmTipoPersonal : Form
    {
        private LTipoPersona _lTipopersona;
        private ETipoPersonal _eTipopersona;

        private bool _isEnablebinding = false;
        public FrmTipoPersonal()
        {
            InitializeComponent();
            _lTipopersona = new LTipoPersona();
            _eTipopersona = new ETipoPersonal();
            _isEnablebinding = true;
            BindingTipopersona();
        }
        public FrmTipoPersonal(ETipoPersonal persona)
        {
            InitializeComponent();
            _lTipopersona = new LTipoPersona();
            _eTipopersona = new ETipoPersonal();

            BindingTipopersonaReload();
            _eTipopersona = persona;
            _isEnablebinding = true;


        }

        private void BindingTipopersonaReload()
        {
            txtTipopersona.Text = _eTipopersona.Nombre;
            

        }
        private void BindingTipopersona()
        {
            if (_isEnablebinding)
            {
                if (_eTipopersona.Nombre == _eTipopersona.Nombre)
                {
                    _eTipopersona.Nombre = _eTipopersona.Nombre;
                }

                _eTipopersona.Nombre = txtTipopersona.Text;
           
            }
        }
        private void Guardar()
        {
            _lTipopersona.GuardarTipopersona(_eTipopersona);
        }
      
        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click_1(object sender, EventArgs e)
        {
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            
        }

        private void FrmTipoPersonal_Load(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click_2(object sender, EventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        { 

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCancelar_Click_1(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click_3(object sender, EventArgs e)
        {
            BindingTipopersona();
            Guardar();
            this.Close();
        }

        private void btnCancelar_Click_2(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
