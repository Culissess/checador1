﻿namespace ChecadorTec
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.agregarTipoPersonalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarPersonalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarTipoPersonalToolStripMenuItem,
            this.agregarPersonalToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // agregarTipoPersonalToolStripMenuItem
            // 
            this.agregarTipoPersonalToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(57)))), ((int)(((byte)(84)))));
            this.agregarTipoPersonalToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(184)))), ((int)(((byte)(184)))));
            this.agregarTipoPersonalToolStripMenuItem.Name = "agregarTipoPersonalToolStripMenuItem";
            this.agregarTipoPersonalToolStripMenuItem.Size = new System.Drawing.Size(136, 20);
            this.agregarTipoPersonalToolStripMenuItem.Text = "Agregar Tipo Personal";
            this.agregarTipoPersonalToolStripMenuItem.Click += new System.EventHandler(this.agregarTipoPersonalToolStripMenuItem_Click);
            // 
            // agregarPersonalToolStripMenuItem
            // 
            this.agregarPersonalToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(57)))), ((int)(((byte)(84)))));
            this.agregarPersonalToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(184)))), ((int)(((byte)(184)))));
            this.agregarPersonalToolStripMenuItem.Name = "agregarPersonalToolStripMenuItem";
            this.agregarPersonalToolStripMenuItem.Size = new System.Drawing.Size(109, 20);
            this.agregarPersonalToolStripMenuItem.Text = "Agregar Personal";
            this.agregarPersonalToolStripMenuItem.Click += new System.EventHandler(this.agregarPersonalToolStripMenuItem_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Principal";
            this.Text = "Principal";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem agregarTipoPersonalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarPersonalToolStripMenuItem;
    }
}