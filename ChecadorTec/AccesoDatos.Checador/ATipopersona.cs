﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConexionBD;
using Entidades.Checador;
using System.Data;

namespace AccesoDatos.Checador
{
    public class ATipopersona
    {
        Conexion _conexion;
        public ATipopersona()
        {
            _conexion = new Conexion("localhost", "root", "", "checadortec", 3306);
        }
        public void EliminarPersona(int idtipopersona)
        {
            string cadena = string.Format("delete from tipopersonal where idpersonalxa = {0} ", idtipopersona);
            _conexion.EjecutarConsulta(cadena);
        }
        public void GuardarPersona(ETipoPersonal persona)
        {
            if (persona.Idpersonal == 0)
            {
                string cadena = string.Format("insert into tipopersonal values(null, '{0}')",persona.Nombre);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("update tipopersonal set nombre = '{0}' where idpersonal = {1}", persona.Nombre, persona.Idpersonal);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public List<ETipoPersonal> ObtenerListaPersona(string filtro)
        {
            var list = new List<ETipoPersonal>();
            string consulta = string.Format("Select * from tipopersonal where idpersonal like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "tipopersonal");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var personal = new ETipoPersonal
                {
                    Idpersonal = Convert.ToInt32(row["idpersonal"]),
                    Nombre = row["nombre"].ToString(),
                };
                list.Add(personal);
            }
            return list;
        }
    }
}
