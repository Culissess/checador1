﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ConexionBD;
using Entidades.Checador;

namespace AccesoDatos.Checador
{
    public class AChecada
    {
        Conexion _conexion;
        public AChecada()
        {
            _conexion = new Conexion("localhost", "root", "", "checadortec", 3306);
        }
        public void GuardarChecada(EChecador checada)
        {
            if (checada.IdChe == 0)
            {
                string cadena = string.Format("insert into checadas values(null, '{0}','{1}','{2}')", checada.Fkrfc, checada.Fecha, checada.Hora);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public List<EChecador> ObtenerChecada()
        {
            var list = new List<EChecador>();
            string consulta = string.Format("Select idche as 'Num Checadas', fkrfc as RFC, Fecha, Hora from checadas");
            var ds = _conexion.ObtenerDatos(consulta, "checadas");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var checador = new EChecador
                {
                    IdChe = Convert.ToInt32(row["Num Checadas"].ToString()),
                    Fkrfc = row["RFC"].ToString(),
                    Fecha = row["Fecha"].ToString(),
                    Hora = row["Hora"].ToString(),
                };
                list.Add(checador);
            }
            return list;
        }
        public string ObtenerNombreChecada()
        {
            string consulta = string.Format("Select idche as 'Num Checadas', fkrfc as RFC, Fecha, Hora from checadas");
            var ds = _conexion.ObtenerDatos(consulta, "checadas");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var checador = new EChecador
                {
                    IdChe = Convert.ToInt32(row["Num Checadas"].ToString()),
                    Fkrfc = row["RFC"].ToString(),
                    Fecha = row["Fecha"].ToString(),
                    Hora = row["Hora"].ToString(),
                };
            }
            return consulta;
        }
    }
}
