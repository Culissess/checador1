﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConexionBD;
using Entidades.Checador;
using System.Data;


namespace AccesoDatos.Checador
{
    public class APersonal
    {
        Conexion _conexion;
        public APersonal()
        {
            _conexion = new Conexion("localhost", "root", "", "checadortec", 3306);
        }
        public void EliminarPersonal(string rfc)
        {
            string cadena = string.Format("delete from personal where rfc = '{0}'", rfc);
            _conexion.EjecutarConsulta(cadena);
        }
        public int ObtenerRfc(string rfc)
        {
            var res = _conexion.Existencia("select count(*) from personal where rfc='" + rfc + "'");
            return res;
        }
        public void GuardarPersonal(EPersonal personal)
        {
            if (ObtenerRfc(personal.Rfc) == 0)
            {
                string cadena = string.Format("insert into personal values('{0}','{1}','{2}','{3}','{4}',{5})", personal.Rfc, personal.Nombre, personal.ApellidoP, personal.ApellidoM, personal.Fechadenacimiento, personal.Fktipopersonal);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("update personal set nombre = '{0}', apellidoP = '{1}', apellidoM = '{2}', fechadenacimiento = '{3}', fktipopersonal = {4} where rfc = '{5}'", personal.Nombre, personal.ApellidoP, personal.ApellidoM, personal.Fechadenacimiento, personal.Fktipopersonal, personal.Rfc);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public List<EPersonal> ObtenerListaPersonal(string filtro)
        {
              var list = new List<EPersonal>();
              string consulta = string.Format("Select * from personal where rfc like '%{0}%'", filtro);
              var ds = _conexion.ObtenerDatos(consulta, "personal");
              var dt = ds.Tables[0];

              foreach (DataRow row in dt.Rows)
              {
                  var personal = new EPersonal
                  {
                      Rfc = row["rfc"].ToString(),
                      Nombre = row["nombre"].ToString(),
                      ApellidoP = row["apellidoP"].ToString(),
                      ApellidoM = row["apellidoM"].ToString(),
                      Fechadenacimiento = Convert.ToDateTime(row["fechadenacimiento"].ToString()),
                      Fktipopersonal = Convert.ToInt32(row["fktipopersonal"].ToString()),
                  };
                  list.Add(personal);
              }
              return list;
            }
    }
}
