﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Checador
{
    public class ETipoPersonal
    {
        private int idpersonal;
        private string nombre;

        public int Idpersonal { get => idpersonal; set => idpersonal = value; }
        public string Nombre { get => nombre; set => nombre = value; }
    }
}
