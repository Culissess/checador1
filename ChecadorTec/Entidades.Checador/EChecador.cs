﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Checador
{
    public class EChecador
    {
        private int idChe;
        private string fkrfc;
        private string fecha;
        private string hora;

        public int IdChe { get => idChe; set => idChe = value; }
        public string Fkrfc { get => fkrfc; set => fkrfc = value; }
        public string Fecha { get => fecha; set => fecha = value; }
        public string Hora { get => hora; set => hora = value; }
    }
}
